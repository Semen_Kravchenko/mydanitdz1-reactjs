import React, { Component } from 'react';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import "./App.css"

class App extends Component {
  state = {
    setModalActive: false,
  }

  openModal = (id) => {
    this.setState({setModalActive: id})
  }

  closeModal = (e) => {
    e.preventDefault();
    this.setState({setModalActive: false})
  }

  render() {
    const {setModalActive} = this.state;
    return (
      <div className="App">
        <Button className="openModal" backgroundColor="red" text="Open first modal" onClick={() => this.openModal('firstModal')} />
        <Button className="openModal" backgroundColor="red" text="Open second modal" onClick={() => this.openModal('secondModal')} />
        <Modal className="firstModal"
               id="firstModal"
               setHide={this.closeModal}
               setActive={setModalActive} 
               text={['Once you delete this file, it won’t be possible to undo this action.', 'Are you sure you want to delete it?']} 
               header='Do you want to delete this file?' 
               actions={[<Button text="Ok" backgroundColor="rgba(0, 0, 0, .3)" onClick={this.closeModal} / >, <Button id="cancel" text="Cancel" backgroundColor="rgba(0, 0, 0, .3)" onClick={this.closeModal} />]}
               closeButton={true}
               />     
        <Modal className="secondModal"
               id="secondModal"
               setHide={this.closeModal}
               setActive={setModalActive} 
               text={['Не знаю, что написать. Плохо с воображением...', 'Однако, данное окно нужно чем-нибудь заполнить, поэтому вот как-то так...']} 
               header='Второе модальное окно' 
               actions={[<Button text="Подтвердить" backgroundColor="rgba(0, 0, 0, .3)" onClick={this.closeModal} / >, <Button id="cancel" text="Отменить" backgroundColor="rgba(0, 0, 0, .3)" onClick={this.closeModal} />]}
               closeButton={true}
        />  
      </div>
    );
  }
}

export default App;
