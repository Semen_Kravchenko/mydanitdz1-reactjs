import React, { Component } from 'react';
import "./Button.scss";

class Button extends Component {

    render() {
        const {backgroundColor, text, onClick, className} = this.props;
        const buttonStyle = {
            backgroundColor: backgroundColor,
        }

        return (
            <button className={`button${className ? " button__" + className : ''}`} onClick={onClick} style={buttonStyle}>{text}</button>
        );
    }
}

export default Button;
