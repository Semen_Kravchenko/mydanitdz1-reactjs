import React, { Component } from 'react';
import './Modal.scss';

class Modal extends Component {
    insertText(text) {
        const textElement = text.map((e, i) => {
            return <p id={'body-text-str-' + i}>{e}</p>
        });
        return textElement;
    }

    render() {
        const { header, text, closeButton, actions, setActive, setHide, id, className } = this.props;
        return (
            <div id={id} className={setActive === id ? "modal active" : "modal"} onClick={setHide}>
                <div className={`modal__content${className ? " modal__" + className : ''}`} onClick={(e) => e.stopPropagation()}>
                    <div className="modal__header">
                        <h1 className="modal__header-title">
                            {header}
                        </h1>
                        {closeButton ? <a href="#" className="modal__close" onClick={setHide}>{String.fromCharCode(0x2716)}</a> : null}   
                    </div>
                    <div className="modal__body">{this.insertText(text)}</div>
                    <div className="modal__buttons">
                        {actions}
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;